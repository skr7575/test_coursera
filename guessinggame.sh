#!/usr/bin/env bash
# File: guessinggame.sh

dirarray=($(ls -d */))
dircount=${#dirarray[@]}
dirguess=0

function guesscheck {
    #if statement
    if [[ $1 -lt $2 ]]
    then
    echo "echo Too low"
    elif [[ $1 -gt $2 ]]
    then
    echo "echo Too high"
    fi
}

# Loop
while [ $guess -ne $dircount ]
do
echo "Guess the number of files in the current directory"
# user response
read guess

$(guesscheck $guess $dircount)

done
echo "Congratulations, you nailed it!"
